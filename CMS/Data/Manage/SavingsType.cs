﻿using CMS.Models;
using Newtonsoft.Json;
using System.Net;

namespace CMS.Data.Manage
{
    public class SavingsType
    {
        public static List<SavingsTypeModel> getAll()
        {
            var responseModel = new List<SavingsTypeModel>();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5176/api/Savings/get-savings-type";
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                responseModel = JsonConvert.DeserializeObject<List<SavingsTypeModel>>(apiResultModel.data.ToString());
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    return responseModel;
                }
            }
            return responseModel;
        }
    }
}
