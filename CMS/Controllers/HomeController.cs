﻿using CMS.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;

namespace CMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public  IActionResult Index()
        {
            string Message = string.Empty;
            if (HttpContext.Session.GetString("email") == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var responseModel = new CustomerDTOModel();
            HttpClient client = new HttpClient();
            string email = HttpContext.Session.GetString("email");
            string uri = "http://localhost:5213/api/Customer/" + email;
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                responseModel = JsonConvert.DeserializeObject<CustomerDTOModel>(apiResultModel.data.ToString());
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    Message = responseModel.Name;
                }
            }
            ViewBag.ErrorMessage = Message;
            return View(responseModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}