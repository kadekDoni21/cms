﻿using CMS.Data.Manage;
using CMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace CMS.Controllers
{
    public class SavingsController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("email") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            SavingsAccountResponse savingsAccountResponse = new SavingsAccountResponse();

            var customer = Data.Manage.Customer.GetByEmail(HttpContext.Session.GetString("email"));
            if (customer == null || (customer.SavingsAccountID == new Guid() || customer.SavingsAccountID == Guid.Empty))
            {
                return View();
            }

            //get list savingAccounts
            savingsAccountResponse = Data.Manage.SavingsAccounts.GetByID(customer.SavingsAccountID);
            if (savingsAccountResponse == null)
            {
                return View();
            }

            return View(savingsAccountResponse);
        }

        [HttpPost]
        public ActionResult Delete(SavingsAccountResponse model)
        {
            if (HttpContext.Session.GetString("email") == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var customer = Data.Manage.Customer.GetByEmail(HttpContext.Session.GetString("email"));

            UpdateCustomerSavingsAccountIdModel updateCustomerSavingsAccountIdModel = new UpdateCustomerSavingsAccountIdModel
            {
                CustomerID = customer.ID,
                SavingsAccountID = model.SavingsAccount.ID
            };
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(updateCustomerSavingsAccountIdModel);
            var requestAPI = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5176/api/Savings/delete-savings-account");
            requestAPI.Content = new StringContent(json, Encoding.UTF8);
            requestAPI.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = client.Send(requestAPI);
            var content = response.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    return RedirectToAction("Index", "Savings");
                }
                else
                {
                    return RedirectToAction("Index", "Savings");
                }
            }

            return View("Index");
        }

        [HttpGet]
        public IActionResult Add()
        {
            PopulateSavingsType();
            return View();
        }

        [HttpPost]
        public IActionResult Add(SavingsAccountModel model)
        {
            var customer = Data.Manage.Customer.GetByEmail(HttpContext.Session.GetString("email"));
            model.CustomerID = customer.ID;
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(model);
            string errorMessage = string.Empty;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5176/api/Savings/add-savings-account");
                request.Content = new StringContent(json, Encoding.UTF8);
                request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.Send(request);
                var content = response.Content.ReadAsStringAsync();
                var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

                if (apiResultModel != null)
                {
                    if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                    {
                        return RedirectToAction("Index", "Savings");
                    }
                    else
                    {
                        errorMessage = "Failed to add savings account";
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Error";
            }

            ViewBag.Error = errorMessage;

            PopulateSavingsType();
            return RedirectToAction("Add", "Savings");
        }

        public void PopulateSavingsType()
        {
            var savingType = Data.Manage.SavingsType.getAll();

            List<SelectListItem> SavingTypeList = new List<SelectListItem>();

            ViewBag.Testing = savingType;

            foreach (var saving in savingType)
            {
                SavingTypeList.Add(new SelectListItem { Text = saving.Name, Value = saving.ID.ToString() });
            }
            ViewBag.SavingType = SavingTypeList;
        }
    }
}
