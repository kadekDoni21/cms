﻿namespace CMS.Models
{
    public class CustomerDetailModel
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid SavingsAccountID { get; set; }
    }
}
