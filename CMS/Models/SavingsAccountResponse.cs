﻿using CMS.Data.Manage;

namespace CMS.Models
{
    public class SavingsAccountResponse
    {
        public SavingsAccountModel SavingsAccount { get; set; }
        public SavingsTypeModel SavingType { get; set; }
    }
}
