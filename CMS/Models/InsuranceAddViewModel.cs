﻿namespace CMS.Models
{
    public class InsuranceAddViewModel
    {
        public List<Models.Insurance> insurances { get; set; }
        public List<string> insuranceID { get; set; }
    }
}
