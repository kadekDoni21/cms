﻿namespace CMS.Models
{
    public class AddCustomerInsuranceRequest
    {
        public Guid customerID { get; set; }
        public List<string> InsurancesID { get; set; }
    }
}
